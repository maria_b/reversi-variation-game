namespace EightNeighbourReversi
{
    /**
    Result enum stores possible game result values
    *
    * @author  Maria Barba 
    * @version 1.0
    * @since   2021-09-15
    */
  public enum Result 
  {
      RED, 
      WHITE,
      TIE
  }
 }