//Maria Barba 
using System;

namespace EightNeighbourReversi
{
   /**
    * Disc enum references the type of value a board space can be,
    * it can be either EMPTY,RED or WHITE.
    *
    * @author  Maria Barba 
    * @version 1.0
    * @since   2021-09-15
    */
  public enum Disc 
  {
    EMPTY,
    RED,
    WHITE 
   
  }
}
