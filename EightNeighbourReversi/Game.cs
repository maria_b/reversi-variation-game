
using System;

namespace EightNeighbourReversi
{
    
    /**
    * Game class stores functionality of the game. It has 
    * two fields for each player, A Game constructor, and 
    * amethod play that controls the flow of the game.
    *
    * @author  Maria Barba 
    * @version 1.0
    * @since   2021-09-15
    */
    public class Game
    {
    /**reference the two IPlayer interfaces as fields
     (one for humanPlayer and botPlayer ) **/   
    private  IPlayer playerOne;
    private  IPlayer playerTwo;  
    
    /**
    * The constructor for a Game object should takes as 
    * input two interface objects and uses them to set the fields
    * @param IPlayer playerOne,IPlayer playerTwo
    * @return Game object.
    */
    public Game(IPlayer playerOne,IPlayer playerTwo)
    {
        this.playerOne=playerOne;
        this.playerTwo=playerTwo;
    
    }
    /**
    * Play() method allows the user to specify the size of the board and creates 
    * the Board object. This method controls the flow of an entire 
    * game, alternating between each player’s turn (by calling their 
    * ChooseMove() method) as well as checking if anyone has won. 
    T* he method should return a Result enum representing who won 
    * the game (so either RED, WHITE, or TIE).
    * @param no args
    * @return Result object.
    */
    public Result Play()
    {
        //allows the user tospecify the size of the board
        Console.WriteLine("Enter the size of the board. - ");
        int boardSize = int.Parse(Console.ReadLine());
        Console.WriteLine("The size of the board entered is : {0}",boardSize);
        //create a Board object
        Board GameBoard = new Board(boardSize);
        //create a copy of the board object 
        Board GameBoardCopy = new Board(GameBoard);
        //calls the ChooseMove method alternating between each player
        //as long as no one has won, continue the game
        
        //while there are empty spaces on the board play the game
        while(Board.CheckFullBoard())
        {
            Console.WriteLine("While the board has empty spaces,play -");
            //call the ChooseMove method for human player
            playerOne.ChooseMove(GameBoardCopy);
            //call the ChoseMove for the bot player
            playerTwo.ChooseMove(GameBoardCopy);
        }
        //create a Result
        Result gameResult = new Result();
        //assign value to the Result
        gameResult=Board.GetWinner();
        //Return the result value
        return gameResult;
    }

    }
  
}