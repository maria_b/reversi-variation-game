
using System;
    
namespace EightNeighbourReversi
{
    /**
    * IPlayer interface is a basis for HumanPlayer and Botplayer
    *
    * @author  Maria Barba 
    * @version 1.0
    * @since   2021-09-15
    */
    public interface IPlayer
    {
        //provides a getter for MyDisc
        Disc MyDisc
        {
            get;
        }
        /**
        * ChooseMove  returns a new Position representing the row and 
        * column where the player wants to place their disc.
        * @param Board board
        * @return Position of Disc.
        */
        Position ChooseMove(Board board);
    }
    
    /**
    * HumanPlayer inherits from IPlayer interface.
    *
    * @author  Maria Barba 1932657
    * @version 1.0
    * @since   2021-09-15
    */
    public class HumanPlayer : IPlayer
    {
        //Provides a getter for MyDisc
        public Disc MyDisc
        {
            get;
        }
        
         /**
         * Constructor for HumanPlayer that takes input
         * a Disc.
         * @param Disc humanDisc
         * @return HumanPlayer object.
         */
        public HumanPlayer(Disc humanDisc)
        {
           this.MyDisc=humanDisc;
           //sets the Disc with the provided Disc
           Console.WriteLine("Human Disk is {0} -",humanDisc);
        }

         /**
         * ChooseMove takes as input a Board object and returns a Position
         * if the input value is valid.Throws an Exception is the board is full.
         * a Disc.
         * @param Board board
         * @return Position pos of the palyer's Disc.
         * @exception InvalidOperationException when board is full
         */
        public Position ChooseMove(Board board)
        {
            //Display the board to the user
            Console.WriteLine(board);

            /**Then check if the board is full, if yes, throw an exception. 
            If not, ask the player which space they would like to choose and 
            return that Position. **/
            
            if (!(Board.CheckFullBoard()))
            {
                throw new InvalidOperationException("The board is already full");
            }
            else
            {
                //Get user position
                Console.WriteLine("Enter a position on the board - ");
                Console.WriteLine("Row Position - ");
                int row = int.Parse(Console.ReadLine());
                Console.WriteLine("Column Position - ");
                int column = int.Parse(Console.ReadLine());
                Console.WriteLine("Row '{0}' Column '{1}'", row, column);
                //While the board position is not valid, ask the user to input another board positionS
                while (!(Board.PlaceDisc(row, column, MyDisc)))
                {
                    Console.WriteLine("The input position is not valid !");
                    Console.WriteLine("Enter a position on the board - ");
                    Console.WriteLine("Row Position - ");
                    row = int.Parse(Console.ReadLine());
                    Console.WriteLine("Column Position - ");
                    column = int.Parse(Console.ReadLine());
                    Console.WriteLine("Row '{0}' Column '{1}'", row, column);
                }
                 //Console.WriteLine(board);
                //When the user input a position that exists on the board, create and return a Position object
                Position userPos = new Position(row, column);
                return userPos;
            }

        }

    }
    /**
    * BotPlayer inherits from IPlayer interface.
    *
    * @author  Maria Barba 1932657
    * @version 1.0
    * @since   2021-09-15
    */
    public class BotPlayer : IPlayer
    {
        //Provides a getter for MyDisc
        public Disc MyDisc
        {
            get;
        } 
         /**
         * Constructor for BotPlayer that takes input
         * a Disc.
         * @param Disc humanDisc
         * @return HumanPlayer object.
         */
         public BotPlayer(Disc botDisc)
        {
           this.MyDisc=botDisc;
           //sets the MyDisc value
           Console.WriteLine("Bot Disk is {0} -",MyDisc);
        }
         /**
         * ChooseMove takes as input a Board object and returns a Position
         * if the input value is valid.
         * a Disc.
         * @param Board board
         * @return Position pos of the palyer's Disc.
         */
        public Position ChooseMove(Board board)
        {
            /**The BotPlayer simply finds the first legal position available. In the
            BotPlayer’s ChooseMove method, you should loop over the board and choose
             a position to place the disc**/
            int row=0;
            //stores a row value
            int column=0;
            //stores a column value

            Console.WriteLine("It's the bot player turn to play !");
            Console.WriteLine(board);
            bool exitedInnerLoop = false;
            //stores a value to check if the inner loop has been exited
            for ( row = 0; row < board.Disk2DArray.GetLength(0); row++)
            {
                //loop through rows
                for ( column = 0; column < board.Disk2DArray.GetLength(1); column++)
                {
                    //if the position is valid place the Disc on the position 
                    if (Board.PlaceDisc(row, column, MyDisc))
                    {
                        Console.WriteLine("Bot player placed disc at row - {0} column - {1}", row, column);
                        exitedInnerLoop=true;
                        //set that the inner loop has been set to true
                        break;
                        //break from inner loop
                    }
                }
                if(exitedInnerLoop)
                {
                    break;
                    //if the inner loop has been exited, break the outer loop.
                }
            }
            //return bot position
            
            //Create a Position object and return it
            Position botPos=new Position(row,column);
            return botPos;
        }
    }

}