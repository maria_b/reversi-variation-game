using System;

namespace EightNeighbourReversi
{
    /**
    * NeighbourReversiMain stores the main 
    * functionality of the game.It creates
    * two player objects (One for Botplayer and one for HumanPlayer)
    * It uses the created Game object to play a game of 8 Neighbour
    * Reversi and returns the result of the Game.
    *
    * @author  Maria Barba 
    * @version 1.0
    * @since   2021-09-15
*/
 
    public class NeighbourReversiMain
    {
        static void Main(string[] args)
        {
        //Give the White Disc color to the HumanPlayer
        IPlayer playerOne=new HumanPlayer(Disc.WHITE);
        //Give the Red Disc color to the Botplayer
        IPlayer playerTwo=new BotPlayer(Disc.RED);
        //Create a Game object and assign player1 and player2
        Game GameObj=new Game(playerOne,playerTwo);
        Result gameRes = GameObj.Play();
        //play the game
        Console.WriteLine("{0} won !",gameRes);
        }
        
    }
    

}
