
using System;

namespace EightNeighbourReversi
{

    /**
    Position class refernces the Position of a Disc on the game
    board. It has a property of Row, Column that specifically 
    points on a space on the board.
    *
    * @author  Maria Barba 
    * @version 1.0
    * @since   2021-09-15
    */
    public class Position
    {
        //Property Row has a getter
        public int Row { get; }
        //Property Column has a getter
        public int Column { get; }
        /*
        * Position constructor sets the property Row and Column
        *
        * @input int row,int column
        * @return Position object
        */
        
        public Position(int row, int column)
        {
            this.Row = row;
            this.Column=column;
        }
    }

    }

