
using System;

namespace EightNeighbourReversi
{
    /**
    The class Board has a  field for Disc Two Dimensional array,
    a field to store the board size and properties Disk2DArray,BoardSize
    .This class stores the functionality of the board
    *
    * @author  Maria Barba 
    * @version 1.0
    * @since   2021-09-15
    */
    public class Board
    {
         private  static Disc[,] disk2DArray;
         //store a 2-D array of disk
         private int boardSize;
         //create a variable to store the board size 
         public Disc[,] Disk2DArray { get { return disk2DArray; } } 
         //property Disk2DArray gets the disk2DArray
         public int BoardSize { get { return boardSize; } }
        
        /**
        * This is an indexer (i.e. overloading the [ , ] operator) 
        * that gets the Disc at a given row and column.
        * @param int row, int column.
        * @return Disc of disk2DArray[row,column]
        */
        public Disc this[int row, int column] => disk2DArray[row,column];
          
        /**
        * Board constructor takes the board size as input and
        * creates a 2D array of Discs. Initially each position 
        * should be set to EMPTY except for the four corners of
        * the board. The four corners of the board must contain 
        * two red and white discs.
        * @param int boardSize
        * @return no args
        */
         public Board(int boardSize)
        {
            disk2DArray = new Disc[boardSize, boardSize];
            //create array of Discs from provided size
            for(int row = 0; row < disk2DArray.GetLength(0); row++)
            {
                //loop through rows
                for(int column = 0; column < disk2DArray.GetLength(1); column++)
                {
                    
                    //loop through columns
                    if(row==0 && column==0)
                    {
                        disk2DArray[row,column] = Disc.RED;
                        //set this position RED
                    }
                    else if(row==boardSize-1 && column==0)
                    {
                        disk2DArray[row,column] = Disc.WHITE;
                        //set the position WHITE
                    }
                    else if(row==0 && column==boardSize-1)
                    {
                        disk2DArray[row,column] = Disc.RED;
                        //set the position RED
                    } 
                    else if(row==boardSize-1 && column==boardSize-1)
                    {
                        disk2DArray[row,column] = Disc.WHITE;
                        //set the position WHITE
                    } 
                    else
                    {
                      continue;      
                    }
                }    
            }
        }
        
        /**
        * isOnBoard takes as input a row,column and size for the
        * board. It checks if the input position is actually on the 
        * game board. Returns true if it's on the board and returns
        * false if it's not on the board.
        * @param int row, int column, int size
        * @return no bool (true = if val on board, false = if val not on board)
        */
        public static bool isOnBoard(int row,int column,int size)
        {
            if((row>=0 && row<size) && (column>=0 && column<size))
            {
                //if on the board, returns true
                return true;
            }
            else{
                //if not on the board,returns false
                return false;
            }
        }
        
        /**
        *   The PlaceDisc method takes the row, column and the disc 
        *   to place and returns a bool if the disc has been placed
        *   successfully. This method does the following:
        *   o Checks if the given position is empty and legal 
        *   (that has at least one disc of the same value as toPlace 
        *   in any one of its 8 neighbours)
        *   o If yes, updates the position to hold the disc.
        *   o Then converts all the neighbouring discs to same value 
        *   as the disc you just placed. And return true.
        *   o Returns false if the disc cannot be placed on the 
        *   given position.
        *   @param int row, int column, Disc toPlace
        *   @return no bool (true = if succsessfully placed, false = if not placed)
        *   @exception IndexOutOfRangeException on surrounding Disc position not on the board.    
        */
        
        public static bool PlaceDisc(int row, int column, Disc toPlace) 
        {
                bool resultPlace=false;
                bool atLeastOneValSame=false;
                //first verify that the position actually exist on the board
                if (!(isOnBoard(row, column,disk2DArray.GetLength(0)))) 
                {
                    return resultPlace;
                }
                //find pos of 8 neighbor values 
                (int X, int Y) [] neighborValues = new [] {(0, 1), (1, 1), (1, 0), (1, -1), (0, -1), (-1, -1), (-1, 0), (-1, 1)};    
                //check if the position is empty
                if(disk2DArray[row,column] == Disc.EMPTY)
                    {
                    //check if the given position has at least one disc of the same value as toPlace
                    //in any of its 8 values    
                   
                        foreach ((int xVar, int yVar) in neighborValues)
                        { 
                            //loop through neighbouring values
                            var x=row;
                            var y=column;
                            x=xVar+x;
                            y=yVar+y;
                            //if the neighbouring (x,y) value is on the board
                            //and there is at least one neighbouring value of the same
                            //color as toPlace
                              
                               
                                try
                                {
                                        if(disk2DArray[x,y]==toPlace && isOnBoard(x,y,disk2DArray.GetLength(0)))
                                        {
                                        atLeastOneValSame=true;
                                        
                                        } 
                                }catch(IndexOutOfRangeException e)
                                {
                                    continue;
                                    
                                }
                                   
                                if(atLeastOneValSame){
                                
                                disk2DArray[row,column]=toPlace;
                                //set Disc on the input row and column
                                 
                                if(disk2DArray[x,y]is not Disc.EMPTY )
                                {  
                                disk2DArray[x,y]=toPlace;
                                resultPlace=true;
                                 //convert the neighbouring disks to same value 
                                }
                            }
                            
                        }
                        
                        //returns true (position is valid) if all the conditions are met
                    }     
                    return resultPlace;
        }
            /**
            * A method GetWinner that takes no inputs and returns a Result representing 
            * who won. The winner is determined by checking which player has a higher 
            * number of discs on the board. If both players have the same number of discs,
            * then the game is a tie.
            * @param args Unused.
            * @return Result (Who won the game RED/WHITE/TIE).
            */ 
            public static Result GetWinner()
            {
                int redDiskCount=0;
                //redDiskCount keeps track of the red disks
                int whiteDiskCount=0;
                //whiteDiskCount keeps track of the white disks

               //loop through board and count amount of each Disc
               foreach (Disc i in disk2DArray)
                {
                    if(i==Disc.RED)
                    {
                        redDiskCount++;
                    } 
                    else if(i==Disc.WHITE)
                    {
                        whiteDiskCount++;
                    }  
                    else
                    {
                        //continue the loop if a position is EMPTY
                         continue;
                    } 
                }
               
                //check who won. Red player wins if there are more red
                //than White Discs.
                if(redDiskCount>whiteDiskCount)
                {
                    return Result.RED;
                }
                //If there are more White discs than Red Discs,WHITE player wins
                else if(redDiskCount<whiteDiskCount)
                {
                    return Result.WHITE;
                }
                else
                //If the amount is equal then return a result of TIE
                {
                    return Result.TIE;
                }
                

            } 
            /**
            * Copy constructor of the board class returns a deep copy of Board obj.
            * @param args Board b.
            * @return Board obj copy of b.
            */
            
            public Board(Board b)
            {
                boardSize=b.BoardSize;
                //create a copy of array of discs and boardsize
                //loop through the old array and set values to new array
                for(int row = 0; row < boardSize; row++)
                {
                //loop through rows
                    for(int column = 0; column < boardSize; column++)
                    {
                        Console.WriteLine("Row : {0} Column {1}  Count {2}",row,column,GetWinner());
                        disk2DArray[row,column]=b.Disk2DArray[row,column];
                        //set values correctly to copy of disk2DArray
                    }
                }
               
                
               
            }
            
            /**
            * Override the ToString() method returns  a rectangle of the board where each
            * RED is displayed as an R, each WHITE is displayed as a W, and each 
            * blank is displayed as a hyphen 
            * @param args Unused.
            * @return String of Board object.
            */
            public override String ToString() 
            {  
                //create a variable to store the delimeter of columns
                 string colDelimeter="======================";
                 //wholeBoard will store a string value of board
                 string wholeBoard="";
                 for(int row = 0; row < disk2DArray.GetLength(0); row++)
                 {
                //loop through rows
                        for(int column = 0; column < disk2DArray.GetLength(1); column++)
                        {
                            //loop through columns
                            Disc boardCell=disk2DArray[row,column];
                            //save the current position on the Board
                            string boardValue;
                            if(boardCell==Disc.RED)
                            {
                                //if at current position is Red, display R
                                boardValue="R";
                            }
                            else if(boardCell==Disc.WHITE)
                            {
                                //if at current position is White, display W 
                                boardValue="W";    
                            }
                            else
                            {
                                //if it's empty, display a hyphen
                                boardValue="-";
                            }
                            wholeBoard=wholeBoard+boardValue;
                        }
                        //add a delimeter line for every column
                        wholeBoard=wholeBoard+Environment.NewLine+colDelimeter+Environment.NewLine;
                 }   
                 return wholeBoard;         
            }

            /**
            * CheckFullBoard returns true if there are empty spaces on the board
            * and returns false if there are no empty spaces on the board
            * @param args Unused.
            * @return bool (true= if empty spaces on board, false=if no empty spaces).
            */
            public static  bool CheckFullBoard(){
                //set the default result to be false
                var result=false; 
                //loop through the board and check if there is an Empty space
                foreach(Disc i in disk2DArray)
                {
                    if(i==Disc.EMPTY)
                    {
                       result=true;     
                       //returns true if there is an empty space on the board
                    }
                }
                return result;
            }
        }
        
    }    
  