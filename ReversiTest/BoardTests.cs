using EightNeighbourReversi;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace ReversiTest
{
     /**
    The class BoardTests tests the PlaceDisc method from the Board class.
    *
    * @author  Maria Barba
    * @version 1.0
    * @since   2021-09-15
    */
    [TestClass]
    public class BoardTests
    {
        //Test PlaceDisk when the input does not exist on board
        [TestMethod]
        public void PlaceDisc_With_Input_NotOnBoard()
        {
           //Arrange
           Board testBoard=new Board(8);
           //Create a game board 
            int row=9;
            //set a row of 9
            int column=0;
            //set a column of 0
            Disc testDisc =Disc.RED;
            //create a RED disc
            bool expected=false;
            //provide an expectend answer that is false
            //Act
            bool actual=Board.PlaceDisc(row,column,testDisc);
          
            //Assert 
            Assert.AreEqual(expected, actual, "PlaceDisc did not recognize value not on board.");

        }
        //Test PlaceDisc when the input has a neighbor of the same color
        [TestMethod]
        public void PlaceDisc_With_Input_OnBoard_Neighbor()
        {
           //Arrange
           Board testBoard=new Board(6);
           //Create a game board 
            int row=5;
            //set a row of 5
            int column=1;
            //set a column of 1
            Disc testDisc =Disc.WHITE;
            //create a WHITE disc
            bool expected=true;
            //provide an expectend answer that is true
            //Act
            bool actual=Board.PlaceDisc(row,column,testDisc);
            //Assert 
            Assert.AreEqual(expected, actual, "PlaceDisc assumed row - 5 , column -1 is not valid.");

        }
        //Test when the Disc placed on the board has no neighbor of the same value surrounding it
        [TestMethod]
        public void PlaceDisc_With_Input_OnBoard_NotNeighbor()
        {
           //Arrange
           Board testBoard=new Board(4);
           //Create a game board 
            int row=2;
            //set a row of 2
            int column=2;
            //set a column of 2
            Disc testDisc =Disc.RED;
            //create a RED disc
            bool expected=false;
            //provide an expectend answer that is false
            //Act
            bool actual=Board.PlaceDisc(row,column,testDisc);
    
            //Assert 
            Assert.AreEqual(expected, actual, "PlaceDisc assumed row - 2 , column -2 is valid.");

        }
        
        //Test PlaceDisc when the position at input is not empty
        [TestMethod]
        public void PlaceDisc_With_InputPos_NotEmpty()
        {
           //Arrange
           Board testBoard=new Board(7);
           //Create a game board 
            int row=6;
            //set a row of 6
            int column=0;
            //set a column of 0
            Disc testDisc =Disc.WHITE;
            //create a RED disc
            bool expected=false;
            //provide an expectend answer that is false
            //Act
            bool actual=Board.PlaceDisc(row,column,testDisc);
            //Assert 
            Assert.AreEqual(expected, actual, "PlaceDisc assumed row - 6 , column -0 is empty.");
        }
        //Test PlaceDisc if the input color cannot make any moves because the opponent color is winning
        [TestMethod]
        public void PlaceDisc_With_InputPos_ColorIsCornered()
        {
           //Arrange
           Board testBoard=new Board(3);
           //Create a game board 
            int row=1;
            //set a row of 1
            int column=2;
            //set a column of 2
            Disc testDisc =Disc.RED;
            //create a RED disc
            bool expected=false;
            //provide an expectend answer that is false
            //Act
            Board.PlaceDisc(1,1,Disc.WHITE);
            bool actual=Board.PlaceDisc(row,column,testDisc);
            //Assert 
            Assert.AreEqual(expected, actual, "PlaceDisc assumed RED can continue making moves.");
        }

         //Test PlaceDisc if the Board is full 
        [TestMethod]
        public void PlaceDisc_With_FullBoard()
        {
           //Arrange
           Board testBoard=new Board(3);
           //Create a game board 
            int row=2;
            //set a row of 2
            int column=1;
            //set a column of 1
            Disc testDisc =Disc.RED;
            //create a RED disc
            bool expected=false;
            //provide an expectend answer that is false
            //Act
            Board.PlaceDisc(1,0,Disc.WHITE);
            Board.PlaceDisc(0,1,Disc.RED);
            Board.PlaceDisc(1,2,Disc.WHITE);
            Board.PlaceDisc(1,1,Disc.WHITE);
            Board.PlaceDisc(2,1,Disc.WHITE);
            
            bool actual=Board.PlaceDisc(row,column,testDisc);
            //Assert 
            Assert.AreEqual(expected, actual, "PlaceDisc assumed RED can continue put a Disc because Board is not full.");
        }
    }
}
